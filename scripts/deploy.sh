#!/bin/bash
set -e
cd /var/www/archive.iftf.org
git pull
composer install --no-dev --no-progress --optimize-autoloader --apcu-autoloader
npm install
cd web
drush updb --yes
drush cr
