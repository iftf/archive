(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.behaviors.iftfYearGraph = {
    attach: function () {
      var drawGraph = function () {
        if (!drupalSettings.facets || !drupalSettings.facets.sliders || !drupalSettings.facets.sliders.year || !drupalSettings.facets.sliders.year.labels) {
          return;
        }
        var labels = JSON.stringify(drupalSettings.facets.sliders.year.labels);
        if ($('#block-yearchart').data('labels') == labels) {
          return;
        }
        $('#block-yearchart').data('labels', labels);
        var data = [];
        var regex = /(\d+)(<span>)? \((\d+)\)/;
        drupalSettings.facets.sliders.year.labels.forEach(function (element, index) {
          var match = regex.exec(element);
          data.push({ 'count': match[3], 'year': match[1] });
          drupalSettings.facets.sliders.year.labels[index] = match[1] + '<span> (' + match[3] + ')</span>';
        });
        $('#block-yearchart div.block__content svg').remove();
        const svg = d3.select('#block-yearchart div.block__content').append('svg');
        $('#block-yearchart div.block__content svg').height(32).css('margin-left', '16px');
        $('#block-yearchart div.block__content svg').css('width', '100%');
        const width = $('#block-yearchart div.block__content svg').width() - 16;
        const height = 32;
        const chart = svg.append('g');
        const xScale = d3.scaleBand()
          .range([0, width])
          .domain(data.map((s) => s.year))
          .padding(0.1);
        const yScale = d3.scaleLinear()
          .range([height, 0])
          .domain([0, 38]);
        const makeYLines = () => d3.axisLeft()
          .scale(yScale);
        const barGroups = chart.selectAll()
          .data(data)
          .enter()
          .append('g');
        barGroups
          .append('rect')
          .attr('class', 'bar')
          .attr('x', (g) => xScale(g.year))
          .attr('y', (g) => yScale(g.count))
          .attr('height', (g) => height - yScale(g.count))
          .attr('width', xScale.bandwidth());
      };
      drawGraph();
      $(document).once('iftfAjaxViews').ajaxSuccess(function (event, data) {
        drawGraph();
      });
    }
  };
}(jQuery, window.Drupal, window.drupalSettings));
